export interface GDPRRequest extends Readonly<{
    UserUuid: string;
    AgreementVersionUuid: string;
    CenterUuid: string;
    MachineUuid: string;
    SubmitedAt: Date;
    ParentalRelationship: any;
    SigningPersonName: string;
    IsApproved: boolean;
    Deleted: boolean;
    ApprovedAt: Date;
    ApprovedBy: string;
  }> {}

export const getGDPRId = (gdpr: GDPRRequest) => gdpr.UserUuid + '_' + gdpr.AgreementVersionUuid;

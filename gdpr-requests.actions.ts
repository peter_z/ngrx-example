import { ActionTypes } from '../ActionTypes';
import { CreateAction } from '../../flux/Action';
import { GDPRRequest } from 'app/shared/models/gdpr-request';

export const LoadGDPRRequests = CreateAction<GDPRRequest[]>(ActionTypes.GDPR_REQUESTS_LOAD);
export const AddGDPRRequest = CreateAction<GDPRRequest>(ActionTypes.GDPR_REQUEST_ADD);
export const ApproveGDPRRequest = CreateAction<GDPRRequest>(ActionTypes.GDPR_REQUEST_APPROVE);
export const RejectGDPRRequest = CreateAction<GDPRRequest>(ActionTypes.GDPR_REQUEST_REJECT);

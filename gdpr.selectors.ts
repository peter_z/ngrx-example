import { BaseSelectors } from './base.selectors';
import { createSelector } from 'reselect';
import { GDPRRequest } from '../../../shared/models/gdpr-request';

class GDPRRequestSelector extends BaseSelectors {
    public GDPRRequests = createSelector(this.GDPRRequestsState, this.Entities<GDPRRequest>());
}

export const GDPRRequestsSelector = new GDPRRequestSelector();

import { HasEntities } from '../HasEntities';
import { HasLoaded } from '../HasLoaded';
import { HasFilter } from '../HasFilter';
import { GDPRRequest } from 'app/shared/models/gdpr-request';

export interface GDPRRequestsState extends HasEntities<GDPRRequest>, HasLoaded {
}


import { tap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService } from 'angular2-notifications';
import { ApiService } from 'app/core';
import { State, UsersSelector, ComputersSelector } from 'app/core/store';
import { GDPRRequest } from 'app/shared/models/gdpr-request';
import { Observable } from 'rxjs';
import { CenterSelector } from '../core/store/Selectors/center.selectors';
import { GDPRRequestsSelector } from '../core/store/Selectors/gdpr.selectors';
import { UpdateCenterGDPRStatusAction } from '../core/store/centers';
import { User } from '../shared/models/user';
import { Computer } from '../shared/models/computer';
import { AddGDPRRequest, LoadGDPRRequests, ApproveGDPRRequest, RejectGDPRRequest } from 'app/core/store/gdpr-requests';

@Injectable()
export class GDPRService {

    public readonly gdprRequests$: Observable<GDPRRequest[]>
        = this.store.select(GDPRRequestsSelector.GDPRRequests);

    public readonly activeGdprRequestsCount$: Observable<number>
        = this.store
            .select(GDPRRequestsSelector.GDPRRequests)
            .pipe(map((requests) => requests.filter((request) => !request.IsApproved && !request.Deleted).length));

    public readonly isGdprEnabledForCurrentCenter$: Observable<boolean>
        = this.store
            .select(CenterSelector.CenterState)
            .pipe(map((center) =>
                center.centerConfig &&
                center.centerConfig.Gdpr &&
                center.centerConfig.Gdpr.Enabled
            ));

    private users: User[];
    private computers: Computer[];

    public constructor(
        private store: Store<State>,
        private apiService: ApiService,
        private notificationsService: NotificationsService,
        private translateService: TranslateService,
    ) {
        this.loadAwaitingRequests();
        this.setupNewRequestNotification();

        this.store
            .select(UsersSelector.Users)
            .subscribe((users) => this.users = users);

        this.store
            .select(ComputersSelector.Computers)
            .subscribe((pcs) => this.computers = pcs);
    }

    public loadAwaitingRequests() {
        return this.apiService.loadGDPRRequests(false)
            .subscribe((requests) => this.store.dispatch(LoadGDPRRequests(requests)));
    }

    public setCenterGDPRStatus(age: number, centerUuid: string) {
        let gdprEnabled = age === null ? false : true;
        return this.apiService.setCenterGDPRStatus(gdprEnabled, age, centerUuid).pipe(
            tap(() => {
                let ageEditable = age ? false : true;
                this.store.dispatch(new UpdateCenterGDPRStatusAction({ gdprEnabled, age, ageEditable, centerUuid }));
            }));
    }

    public approveGDPRrequest = (gdprRequest: GDPRRequest) =>
        this.apiService.SendRequestAndWaitForResponseFromTempQueue(
            '/topic/approve_signed_agreement_request',
            {
                AgreementVersionUuid: gdprRequest.AgreementVersionUuid,
                UserUuid: gdprRequest.UserUuid
            }
        ).pipe(tap(() => this.store.dispatch(ApproveGDPRRequest(gdprRequest))));

    public rejectGDPRrequest = (gdprRequest: GDPRRequest) =>
        this.apiService
            .SendRequestAndWaitForResponseFromTempQueue(
                '/topic/delete_signed_agreement_request',
                {
                    AgreementVersionUuid: gdprRequest.AgreementVersionUuid,
                    UserUuid: gdprRequest.UserUuid
                }
            ).pipe(tap(() => this.store.dispatch(RejectGDPRRequest(gdprRequest))));

    private setupNewRequestNotification() {
        this.apiService.HandleUpdates('/topic/new_signed_agreement').subscribe((msg) => {
            const request: GDPRRequest = msg.SignedAgreementModel;
            this.store.dispatch(AddGDPRRequest(request));

            const user = this.users.find((u) => u.Uuid === request.UserUuid);
            const computer = this.computers.find((pc) => pc.Uuid === request.MachineUuid);

            if (user && computer) {
                const title = this.translateService.instant('GDPR.NOTIFICATION.TITLE');
                const message = this.translateService.instant('GDPR.NOTIFICATION.MESSAGE', {
                    userName: user.Username,
                    pcName: computer.Name
                });

                this.notificationsService.alert(title, message);
            }
        });
    }
}

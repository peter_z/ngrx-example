
import { takeUntil } from 'rxjs/operators';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { fadeInAnimation } from 'app/shared/animations';
import { Computer } from 'app/shared/models/computer';
import { User } from 'app/shared/models/user';
import 'moment-duration-format';
import { ComputersSelector, State, UsersSelector } from '../core/store';
import { GDPRRequest } from '../shared/models/gdpr-request';
import { GDPRService } from './gdpr.service';
import { ShiftGuard } from 'app/+shifts/shift.guard';
import { Subject } from 'rxjs';

@Component({
    selector: 'glp-settings',
    templateUrl: './gdpr.component.html',
    styleUrls: ['./gdpr.component.scss'],
    animations: [fadeInAnimation],
})
export class GDPRComponent implements OnInit, OnDestroy {

    @ViewChild(DatatableComponent)
    public table: DatatableComponent;

    public gdprRequests: GDPRRequest[] = [];

    public users: { [uuid: string]: User } = {};
    public computers: { [uuid: string]: Computer } = {};

    private _onDestroy = new Subject();

    constructor(
        private gdprService: GDPRService,
        private store: Store<State>,
        private shiftGuard: ShiftGuard,
    ) { }

    public ngOnInit() {
        this.gdprService.gdprRequests$
            .pipe(takeUntil(this._onDestroy))
            .subscribe((gdprRequests) => this.gdprRequests = gdprRequests);

        this.store
            .select(UsersSelector.UsersByIds)
            .pipe(takeUntil(this._onDestroy))
            .subscribe((users) => this.users = users);

        this.store
            .select(ComputersSelector.ComputersByIds)
            .pipe(takeUntil(this._onDestroy))
            .subscribe((computers) => this.computers = computers);
    }

    public ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    public scrollToTop() {
        this.table.bodyComponent.updateOffsetY(0);
    }

    public rejectGDPRrequest(gdprRequest: GDPRRequest) {
        this.shiftGuard.check('common').then(() =>
            this.gdprService.rejectGDPRrequest(gdprRequest).subscribe()
        );
    }

    public approveGDPRrequest(gdprRequest: GDPRRequest) {
        this.shiftGuard.check('common').then(() =>
            this.gdprService.approveGDPRrequest(gdprRequest).subscribe()
        );
    }
}

import { GDPRRequestsState } from './index';
import { ActionTypes } from '../ActionTypes';
import { CreateReducer, HandleAction } from 'app/core/flux/CreateReducer';
import { ArrayToDictionary } from '../helpers';
import { GDPRRequest, getGDPRId } from '../../../shared/models/gdpr-request';

const initialState: GDPRRequestsState = {
    entities: {},
    loaded: false,
};

export const GDPRRequestsReducer = CreateReducer(initialState, {
    [ActionTypes.GDPR_REQUESTS_LOAD]: HandleAction((state, payload: GDPRRequest[]) => {
        return {
            ...state,
            entities: ArrayToDictionary(payload, (i) => getGDPRId(i)),
            loaded: true
        };
    }),

    [ActionTypes.GDPR_REQUEST_ADD]: HandleAction((state, payload: GDPRRequest) =>
        ({ ...state, entities: { ...state.entities, [getGDPRId(payload)]: payload } })),

    [ActionTypes.GDPR_REQUEST_APPROVE]: HandleAction((state, payload: GDPRRequest) => {
        const newEntities = { ...state.entities };
        delete newEntities[getGDPRId(payload)];

        return { ...state, entities: newEntities };
    }),

    [ActionTypes.GDPR_REQUEST_REJECT]: HandleAction((state, payload: GDPRRequest) => {
        const newEntities = { ...state.entities };
        delete newEntities[getGDPRId(payload)];

        return { ...state, entities: newEntities };
    }),
});
